package com.ksy.opticiansmanager.controller;

import com.ksy.opticiansmanager.model.CustomerRequest;
import com.ksy.opticiansmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setData(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(
                request.getCustomerName(),
                request.getCustomerPhone(),
                request.getEyeRight(),
                request.getEyeLeft()
        );

        return "등록되었습니다.";
    }
}
