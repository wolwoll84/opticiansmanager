package com.ksy.opticiansmanager.service;

import com.ksy.opticiansmanager.entity.Customer;
import com.ksy.opticiansmanager.model.CustomerItem;
import com.ksy.opticiansmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(String customerName, String customerPhone, Float eyeRight, Float eyeLeft) {
        Customer addData = new Customer();
        addData.setCustomerName(customerName);
        addData.setCustomerPhone(customerPhone);
        addData.setDateLast(LocalDate.now());
        addData.setEyeRight(eyeRight);
        addData.setEyeLeft(eyeLeft);

        customerRepository.save(addData);
    }
}
