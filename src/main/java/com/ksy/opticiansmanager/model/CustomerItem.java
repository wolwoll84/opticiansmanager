package com.ksy.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerName;
    private String customerPhone;
}
