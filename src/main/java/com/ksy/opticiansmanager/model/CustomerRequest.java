package com.ksy.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 7, max = 20)
    private String customerPhone;

    @NotNull
    private Float eyeRight;

    @NotNull
    private Float eyeLeft;
}
